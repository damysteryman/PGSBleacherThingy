﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using BlurayThingy.PGS;

namespace PGSBleacherThingy
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsolePrint(UI.Banner());

            if (args.Length > 0)
            {
                string src = "";
                string dest = "";
                Tuple<float, float> hueRangeFilter = null;

                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "--help":
                        case "-h":
                            ConsolePrint(UI.Usage());
                            return;

                        case "--src":
                        case "-s":
                            src = args[i + 1];
                            i++;
                            break;

                        case "--dest":
                        case "-d":
                            dest = args[i + 1];
                            i++;
                            break;

                        case "--hue-range-filter":
                        case "-f":
                            string[] filterStr = args[i + 1].Split(':');
                            float num1 = float.Parse(filterStr[0]);
                            if (filterStr.Length > 1)
                            {
                                float num2 = float.Parse(filterStr[1]);
                                float start = Math.Min(num1, num2);
                                float end = Math.Max(num1, num2);

                                hueRangeFilter = new Tuple<float, float>(start, end);
                            }
                            else
                            {
                                hueRangeFilter = new Tuple<float, float>(num1, num1);
                            }  
                            break;
                    }
                }

                if (String.IsNullOrEmpty(dest))
                    dest = src;
                
                if (String.IsNullOrEmpty(src))
                    Console.WriteLine($"ERROR: Input file path not provided, specify it with the '--src' parameter. Exiting...");
                else if (File.Exists(src))
                {
                    Console.WriteLine($"Bleaching {src}...");
                    List<PresentationGraphicsSegment> segments = PGSSerialization.Deserialize(src);
                    foreach (PresentationGraphicsSegment s in segments)
                        if (s.Type == PresentationGraphicsSegment.TYPE.PDS)
                            Bleach((PaletteDefinitionSegment)s, hueRangeFilter);

                    File.WriteAllBytes(dest, PGSSerialization.Serialize(segments));
                    Console.WriteLine($"Finished bleaching {src}, saved to {dest}.");
                }
                else
                    Console.WriteLine($"ERROR: Input file '{src}' not found, exiting...");
            }
            else
                ConsolePrint(UI.Usage());
        }

        public static void Bleach(PaletteDefinitionSegment pds, Tuple<float, float> hueRangeFilter = null)
        {
            foreach (PaletteEntry pe in pds.PaletteEntries)
            {
                int val = Math.Max(pe.PaletteColor.R, pe.PaletteColor.G);
                val = Math.Max(pe.PaletteColor.G, pe.PaletteColor.B);

                if (hueRangeFilter != null)
                {
                    // BLEACH COLOR IF WITHIN HUE RANGE FILTER
                    float hue = pe.PaletteColor.GetHue();
                    if (hue >= hueRangeFilter.Item1 && hue <= hueRangeFilter.Item2)
                    {
                        pe.PaletteColor = System.Drawing.Color.FromArgb(pe.A, val, val,　val);
                        pe.Modified = true;
                    }
                }
                else
                {
                    // BLEACH ALL THE COLORS
                    pe.PaletteColor = System.Drawing.Color.FromArgb(pe.A, val, val,　val);
                    pe.Modified = true;
                }
            }
        }

        private static void ConsolePrint(List<string> stringList)
        {
            foreach (string s in stringList)
                Console.WriteLine(s);
        }
    }
}
