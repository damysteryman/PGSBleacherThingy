using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PGSBleacherThingy
{
    public static class UI
    {
        public static List<string> PutInBorder(this string _str, int _width = -1, bool _startBorder = true, bool _endBorder = true)
        {
            if (_width < 0)
                _width = _str.Length + 4;
                
            List<string> strs = new List<string> { _str };
            strs.PutInBorder(_width, _startBorder, _endBorder);

            return strs;
        }

        public static List<string> PutInBorder(this List<string> _strings, int _width = -1, bool _startBorder = true, bool _endborder = true)
        {
            if (_strings.Count > 0)
            {
                if (_width < 0)
                {
                    _width = _strings.Max(val => val.Length) + 4;
                }

                string border = "=";
                while (border.Length < _width)
                    border += "=";

                for (int i = 0; i < _strings.Count; i++)
                {
                    int len = _width - 1;
                    if (!_strings[i].StartsWith("--"))
                        _strings[i] = " " + _strings[i];
                    _strings[i] = "=" + _strings[i];
                    while (_strings[i].Length < len)
                        if (_strings[i].EndsWith("--"))
                            _strings[i] += "-";
                        else
                            _strings[i] += " ";
                    if (_strings[i].Length == len)
                        _strings[i] += "=";
                }
                if (_startBorder) _strings.Insert(0, border);
                if (_endborder) _strings.Add(border);
            }
            
            return _strings;
        }

        public static List<string> Usage()
        {
            List<string> usage = new List<string>
            { 
                "Usage: PGSBleacherThingy --src <source.sup> --dest <destination.sup> --hue-range-filter <min-value>:<max-value>", ""
            };
            usage.Add("Required arguments:");
            usage.Add("    --src <arg>                  Source .sup file to read in and bleach.");
            usage.Add("");
            usage.Add("Optional arguments:");
            usage.Add("    --dest <arg>                 Destination output file to save bleached subtitle data into.");
            usage.Add("                                 If not specified, output will overwrite input file.");
            usage.Add("    --hue-range-filter <arg>     Hue value range to filter subtitle colors to be bleached. Colors outside this hue range will not be bleached.");
            usage.Add("                                 Format is range of hue values in the form <min-value>:<max-value>");
            usage.Add("                                 Example: using arg 45:70 will only bleach colors in between hue value 45 (roughly orange-yellow),");
            usage.Add("                                          up to hue value 70 (roughly greenish-yellow), beaching basically all yellow colored subtitles.");
            usage.Add("                                 If not specified, all colors will be bleached.");

            return usage;
        }

        public static List<string> Banner()
        {
            string str = $"Ver: {((AssemblyInformationalVersionAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyInformationalVersionAttribute)))).InformationalVersion}";
            string author = $"By {((AssemblyCompanyAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyCompanyAttribute)))).Company}";
            List<string> banner = new List<string> { "PGSBleacherThingy", str, author };
            banner.PutInBorder();
            return banner;
        }
    }
}